from django.contrib import admin

# Register your models here.
from .models import Mascota,Vacuna

admin.site.register(Mascota)
admin.site.register(Vacuna)