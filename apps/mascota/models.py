from django.db import models
import datetime

# Create your models here.
from django.utils import timezone
from apps.adopcion.models import Persona

class Vacuna(models.Model):
	nombre=models.CharField(max_length=50)
	
	def __str__(self):
		return '{}'.format(self.nombre)

class Mascota(models.Model):
	folio= models.CharField(max_length=10,primary_key=True)
	nombre = models.CharField(max_length=50)
	sexo=models.CharField(max_length=10)
	fecha_rescate = models.DateField(null=True,blank=True)
	edad_aproximada = models.IntegerField(null=True,blank=True)
	fecha_registro = models.DateField(auto_now_add=True)#timezone.now --> DatetimeField-----datetime.date.today
	persona=models.ForeignKey(Persona,null=True,blank=True,on_delete=models.CASCADE)
	vacuna=models.ManyToManyField(Vacuna)

	def __str__(self):
		return '{}'.format(self.nombre)

	#def publish(self):
		#self.fecha_registro = timezone.now()
		#self.save()


