from django.conf.urls import url,include
from django.contrib.auth.decorators import login_required
from apps.mascota.views import listadousuarios, index,mascota_view,mascota_list,mascota_edit,mascota_delete, \
MascotaList,MascotaCreate,MascotaUpdate,MascotaDelete

from . import views

urlpatterns = [
    url(r'^index$', views.index, name='index'),
    url(r'^nuevo/$', login_required(MascotaCreate.as_view()), name='mascota_crear'),
    url(r'^listar/', login_required(MascotaList.as_view()), name='mascota_listar'),
    url(r'^editar/(?P<pk>[\w-]+)/$', login_required(MascotaUpdate.as_view()), name='mascota_editar'),      #'^editar/(?P<cadena>[\w-]+)/$' TEXTO otro <folio>[\w-]+>
    url(r'^eliminar/(?P<pk>[\w-]+)/$', login_required(MascotaDelete.as_view()), name='mascota_eliminar'),#'^editar/(?P<numero>\d+)/$'     NUMERO
    url(r'^listado/', listadousuarios, name="listado"),

]
