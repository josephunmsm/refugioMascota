from django.shortcuts import render,redirect
from django.urls import reverse
from django.contrib.auth.models import User

# Create your views here.
from django.http import HttpResponse
from django.core import serializers
from apps.mascota.form import MascotaForm
from apps.mascota.models import Mascota
from django.views.generic import ListView,CreateView,UpdateView,DeleteView
from django.core.urlresolvers import reverse_lazy
#import pdb

def listadousuarios(request):
	lista = serializers.serialize('json', User.objects.all(), fields=['username', 'first_name'])
	return HttpResponse(lista, content_type='application/json')

def index(request):#pdb.set_trace()
    return render(request,'mascota/index.html')

def mascota_view(request):
    if request.method=='POST':
        form=MascotaForm(request.POST)
        if form.is_valid():
            form.save()
        else:
            print(form.errors)
            return redirect('mascota:mascota_listar')
    else:
        form=MascotaForm()
    return render(request,'mascota/mascota_form.html',{'form':form})

def mascota_list(request):
    #import pdb
    #pdb.set_trace()
    mascota=Mascota.objects.all().order_by("folio")
    contexto={'mismascotas':mascota}
    return render(request,'mascota/mascota_list.html',contexto)

def mascota_edit(request,folio):
    mascota=Mascota.objects.get(folio=folio)
    if request.method=='GET':
        form=MascotaForm(instance=mascota)
    else:
        form=MascotaForm(request.POST,instance=mascota)
        if form.is_valid():
            form.save()
        return redirect('mascota:mascota_listar')
    return render(request,'mascota/mascota_form.html',{'form':form})

def mascota_delete(request,folio):
    mascota=Mascota.objects.get(folio=folio)
    if request.method=='POST':
        mascota.delete()
        form=MascotaForm(instance=mascota)
        return redirect('mascota:mascota_listar')
    return render(request,'mascota/mascota_delete.html',{'mascota':mascota})

class MascotaList(ListView):
    model=Mascota
    template_name='mascota/mascota_list.html'
    paginate_by=2


class MascotaCreate(CreateView):
    model = Mascota
    form_class=MascotaForm
    template_name='mascota/mascota_form.html'
    success_url=reverse_lazy('mascota:mascota_listar')

class MascotaUpdate(UpdateView):
    model = Mascota
    form_class = MascotaForm
    template_name = 'mascota/mascota_form.html'
    success_url = reverse_lazy('mascota:mascota_listar')

class MascotaDelete(DeleteView):
    model = Mascota
    template_name = 'mascota/mascota_delete.html'
    success_url = reverse_lazy('mascota:mascota_listar')







