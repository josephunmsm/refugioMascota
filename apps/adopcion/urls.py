from django.conf.urls import url
from django.contrib.auth.views import login_required

from apps.adopcion.views import index, SolicitudList, SolicitudCreate, SolicitudUpdate, \
    SolicitudDelete

urlpatterns = [
    url(r'^index$', index),
    url(r'^solicitud/listar$', SolicitudList.as_view(), name='solicitud_listar'),
    url(r'^solicitud/nueva$', SolicitudCreate.as_view(), name='solicitud_crear'),
    url(r'^solicitud/editar/(?P<pk>[\w-]+)/$', SolicitudUpdate.as_view(), name='solicitud_editar'),
    url(r'^solicitud/eliminar/(?P<pk>[\w-]+)/$', SolicitudDelete.as_view(), name='solicitud_eliminar'),

    ]